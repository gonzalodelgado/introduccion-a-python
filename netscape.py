import sys
import mdv
import requests
from html2text import html2text

# sys.argv # ['netscape.py', 'http://lagaceta.com.ar']
if len(sys.argv) > 1:
    direccion = sys.argv[1]
    respuesta = requests.get(direccion)
    if respuesta.status_code == 200:
        print(mdv.main(html2text(respuesta.content.decode('utf-8'))))
    else:
        print('Error del servidor')
