Introducción a Python
=====================

Diapositivas y ejemplos de mi charla de introducción al lenguaje de programación Python.


Notas sobre los ejemplos
------------------------

* `netscape.py` funciona con Python3 y necesita las bibliotecas especificadas en el archivo `requirements.txt`
* `pong.py` requiere [PyGame](https://bitbucket.org/pygame/pygame) y funciona con Python2
