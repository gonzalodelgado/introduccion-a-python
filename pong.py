import math
import random
import pygame



class Pelota:
    cuadro = pygame.rect.Rect(400, 300, 20, 20)
    vector_velocidad = [5, 5]

    def mover(self):
        if self.cuadro.y < 0 or self.cuadro.y + 10 > ventana.get_height():
            self.vector_velocidad[1] *= -1
        if self.cuadro.x < 0 or self.cuadro.x + 10 > ventana.get_width():
            self.reset()
        self.cuadro.move_ip(self.vector_velocidad)

    def reset(self):
        self.cuadro.x, self.cuadro.y = 400, 300
        self.vector_velocidad[0] *= -1

    def colisionar(self, jugador):
        if self.cuadro.colliderect(jugador.paleta):
            angulo = math.atan2(self.vector_velocidad[1], self.vector_velocidad[0])
            angulo += random.uniform(0.1, 0.3)
            magnitud = math.hypot(self.vector_velocidad[0], self.vector_velocidad[1])
            self.vector_velocidad = [math.cos(angulo)*magnitud, math.sin(angulo)*magnitud]
            self.vector_velocidad[0] *= -1


class Jugador:
    ancho = 30
    alto = 150
    velocidad = 10

    def __init__(self, x, y, tecla_arriba, tecla_abajo):
        self.paleta = pygame.rect.Rect(x, y, self.ancho, self.alto)
        self.tecla_arriba = tecla_arriba
        self.tecla_abajo = tecla_abajo

    def mover(self, teclas):
        if teclas[self.tecla_arriba]:
            self.subir()
        if teclas[self.tecla_abajo]:
            self.bajar()

    def subir(self):
        self.paleta.y -= self.velocidad
        if self.paleta.y < 0:
            self.paleta.y = 0

    def bajar(self):
        self.paleta.y += self.velocidad
        if self.paleta.y + self.alto > ventana.get_height():
            self.paleta.y = ventana.get_height() - self.alto


colores = {
    'negro': (0, 0, 0),
    'blanco': (255, 255, 255),
}
ventana = pygame.display.set_mode((800, 600))
pelota = Pelota()
jugador1 = Jugador(x=10, y=100, tecla_arriba=pygame.K_UP, tecla_abajo=pygame.K_DOWN)
jugador2 = Jugador(x=ventana.get_width() - 10 - Jugador.ancho, y=100,
                   tecla_arriba=pygame.K_q, tecla_abajo=pygame.K_a)

while True:
    ventana.fill(colores['negro'])
    pygame.event.pump()
    teclas = pygame.key.get_pressed() # {'arriba': True, 'abajo': False}

    jugador1.mover(teclas)
    jugador2.mover(teclas)
    pelota.colisionar(jugador1)
    pelota.colisionar(jugador2)
    pelota.mover()

    for rectangulo in (pelota.cuadro, jugador1.paleta, jugador2.paleta):
        pygame.draw.rect(ventana, colores['blanco'], rectangulo)
    pygame.display.flip()
